(업데이트 날짜 : 2019.05.14)
-------------------

1. 프로젝트 : 한화 프로젝트
2. 언어 : Android Java
3. 주요 기능 : 한화. NcLink(안드로이드) 의 시스템 Time 세팅
4. 프로세스 동작 : 프로젝트 실행 후, 

(4-1). 한화 유노스 서버 (http://55.111.150.201:8000/getInitTime) 에 HTTP REST (get) 요청

(4-2). 서버 RESTFUL 성공 시, 시스템 시간 변경 후 TimeSetter 프로세스 종료 ( System.exit(0); )

(4-3). 서버 RESTFUL 3회 이상 실패 시, http://localhost:9000/getInitTime 에 HTTP REST (get) 요청

(4-4). http://localhost:9000/getInitTime 는 NcAgent( 담당자 : Jane ) 가 만들어놓은 API 로, 한화의 SynapseNc ( 담당자 : SIT ) 로부터 받은 시간을 Return 함

(4-5). http://localhost:9000/getInitTime 로 시스템 시간 변경 성공시 TimeSetter 프로세스 종료