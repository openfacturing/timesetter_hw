package unomic.com.timesetter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class RebootReceiver extends BroadcastReceiver {
    public RebootReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        //throw new UnsupportedOperationException("Not yet implemented");
        Log.d("RebootReceiver", "action : " + intent.getAction());
        if (Intent.ACTION_BOOT_COMPLETED.equalsIgnoreCase(intent.getAction())) {
            Log.d("start", "Timesetter StartupReceiver success");

            /*
            WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
            if(!wifiManager.isWifiEnabled()) {
                wifiManager.setWifiEnabled(false);
                try {
                    Thread.sleep(1000);
                } catch(Exception ex) {
                    ex.printStackTrace();
                }
                wifiManager.setWifiEnabled(true);
                wifiManager.reconnect();
            //}
            */
            try {
                Thread.sleep(10000);
            } catch(Exception ex) {
                ex.printStackTrace();
            }

            Intent i = new Intent();
            i.setClassName(context.getPackageName(), TimeSetService.class.getName());
            context.startService(i);

            try {
                //Thread.sleep(30000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
