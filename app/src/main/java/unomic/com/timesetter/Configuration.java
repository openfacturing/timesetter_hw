package unomic.com.timesetter;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

/**
 * Created by bong on 2015-09-26.
 */
public class Configuration {
    private static Configuration instance;
    private String server;
    private String agent;
    private String type;
    private String sender;
    private int deviceNum;
    private String timeServer;

    public Configuration(int deviceNum) {
        this.deviceNum = deviceNum;
    }

    // singleton
    public static Configuration getInstance(int deviceNum) {
        if(instance == null)
            instance = new Configuration(deviceNum);

        return instance;
    }

    public void setConfigEMO() {
        String adapterConfigFile = "config.txt";

        if(deviceNum >= 2) {
            adapterConfigFile = "config" + deviceNum + ".txt";
        }

        //String path = Environment.getExternalStorageDirectory().getPath();

        File file = new File("/sdcard/"+adapterConfigFile);
        String str;

        if(file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                byte[] buffer = new byte[(int)file.length()];
                fis.read(buffer);
                str = new String(buffer);
                fis.close();

                JSONObject jsonObject = new JSONObject(str);
                JSONArray jsonArray = jsonObject.getJSONArray("MtCenter");

                Log.d("AdapterConfiguration", jsonArray.toString());

                for(int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    Iterator<String> iterator = jsonObject1.keys();
                    while(iterator.hasNext()) {
                        String key = iterator.next();
                        String value = jsonObject1.getString(key);

                        if(key.equals("request_server_address"))
                            server = value;
                        else if(key.equals("agent_address"))
                            agent = value;
                        else if(key.equals("time_server_address"))
                            timeServer = value;
                        //else if(key.equals("type"))
                        //    type = value;
                    }
                }

                JSONArray jsonArray2 = jsonObject.getJSONArray("MtAgent");
                Log.d("AdapterConfiguration", jsonArray2.toString());
                for(int i = 0; i < jsonArray2.length(); i++) {
                    JSONObject jsonObject1 = jsonArray2.getJSONObject(i);
                    Iterator<String> iterator = jsonObject1.keys();
                    while(iterator.hasNext()) {
                        String key = iterator.next();
                        String value = jsonObject1.getString(key);

                        if(key.equals("device_ip"))
                            sender = value;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            Log.d("ConfigError", "Cannot found File : " + file.getAbsolutePath());
        }
    }

    public String getServer() {
        return server;
    }

    public String getType() {
        return type;
    }

    public String getAgent() {
        return agent;
    }

    public String getSender() {
        return sender;
    }

    public String getTimeServer() {
        return timeServer;
    }
}
