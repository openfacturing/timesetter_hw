package unomic.com.timesetter;

import android.app.AlarmManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

public class TimeSetService extends Service {
    private static final String TAG_LOG = TimeSetService.class.getSimpleName();
    String time = "";
    Context mContext;

    public TimeSetService() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mContext = getApplicationContext();
        MyAsyncTask asyncTask = new MyAsyncTask();

        asyncTask.execute();

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private class MyAsyncTask extends AsyncTask<Void, Void, Void> {
        private static final String TAG_LOG = "MyAsyncTask";
//        private static final String DEFAULT_ADDRESS = "http://106.240.234.116:8080/UNS/mtc/getInitTime.do";
        private String DEFAULT_ADDRESS = "http://55.111.150.201:8000/getInitTime";
//private static final String DEFAULT_ADDRESS = "http://192.168.1.101:9000/getInitTime";
        //private HttpURLConnection conn;
        private InputStream is = null;
        //private URL url, sendUrl;
        private boolean isGet = false;
        int reSetCount = 0;

        @Override
        protected Void doInBackground(Void... params) {
            Configuration configuration = Configuration.getInstance(1);
            configuration.setConfigEMO();

            String timeServerURL = DEFAULT_ADDRESS;
//            if (!configuration.getTimeServer().equals("") && configuration.getTimeServer() != null) {
//                timeServerURL = configuration.getTimeServer();
//            }

            while (!isGet) {
                getTime(DEFAULT_ADDRESS);
                try {
                    Thread.sleep(10000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // 한화 설정.  시간 동기화 후 timesetter 종료. nc_agent app 에 조건에 따라 timesetter 재실행
            System.exit(0);

            return null;
        }

        private void getTime(String timeServerURL) {
            HttpURLConnection conn = null;
            try {
                Log.d(TAG_LOG, "timeServerURL : " + timeServerURL);
                //URL url = new URL("http://192.168.77.100/DKY/mtc/timeTest.do");
                //URL url = new URL("http://106.240.234.116:8080/UNS_SBP/mtc/getInitTime.do");
                URL url = new URL(timeServerURL);
                //URL url = new URL("http://192.168.6.10/UNS/mtc/getInitTime.do");
                //URL url = new URL("http://169.50.18.130:8080/UNS/mtc/getInitTime.do");
                //URL url = new URL("http://10.224.52.113:8080/DULink/adapter/setAdtTime.do");

                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Cache-Control", "no-cache");
                conn.setRequestProperty("Content-Type", "application/text");
                conn.setRequestProperty("Accept", "text/xml");
                conn.setUseCaches(false);
                conn.setDefaultUseCaches(false);
                conn.setConnectTimeout(30000);
                conn.setFixedLengthStreamingMode(0);
                //conn.setRequestProperty("Content-Length", "10000");
                //conn.setDoOutput(true);
                //conn.setDoInput(true);

                int rescode = conn.getResponseCode();
                Log.d(TAG_LOG, "rescode = " + rescode);

                if (rescode == HttpURLConnection.HTTP_OK) {
                    isGet = true;
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();

                    //isConn = true;
                    Log.d(TAG_LOG, "Send XML Post Request Success ret : " + sb.toString());
                    time = sb.toString();
                    Log.d(TAG_LOG, "time : " + time);

                    // DIY CODE
                    //Date date = new Date(Long.valueOf(time.trim()) * 1000L);
                    //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    //time = sdf.format(date);
                    //Log.d(TAG_LOG, "time : " + time);
                    // DIY CODE END

                    conn.disconnect();

                    int year = Integer.valueOf(time.substring(0, 4));
                    int month = Integer.valueOf(time.substring(5, 7));

                    if (month == 0)
                        month = 12;
                    else
                        month = month - 1;

                    int day = Integer.valueOf(time.substring(8, 10));

                    int hour = Integer.valueOf(time.substring(11, 13));
                    int min = Integer.valueOf(time.substring(14, 16));
                    int sec = Integer.valueOf(time.substring(17, 19));

                    Log.d(TAG_LOG, year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec);

                    Calendar c = Calendar.getInstance();
                    c.set(year, month, day, hour, min, sec);
                    AlarmManager am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);

                    String timeZone = c.getTimeZone().getID();
                    Log.d(TAG_LOG, "now TimeZone : " + timeZone);
                    if ("Asia/Seoul".equals(timeZone)) {
                        am.setTime(c.getTimeInMillis());
                    } else if ("Asia/Shanghai".equals(timeZone)) {
                        am.setTimeZone("Asia/Seoul");
                        am.setTime(c.getTimeInMillis() - (3600 * 1000));
                    }
                    //am.setTime(c.getTimeInMillis() - (3600 * 1000));
                    reSetCount = 0;

                    // 한화 ncAgent 폴더에 파일쓰기
                    sync_time_log(time);
                } else {
                    isGet = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG_LOG, e.getMessage());
                String error = e.getMessage();
                /*
                if(error.contains("EHOSTUNREACH")) {
                    Log.d(TAG_LOG, "reconnect wifi");
                    WifiManager wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
                    wifiManager.setWifiEnabled(false);
                    try {
                        Thread.sleep(5000);
                    } catch(Exception ex) {
                        ex.printStackTrace();
                    }
                    wifiManager.setWifiEnabled(true);
                    wifiManager.reconnect();
                    Log.d(TAG_LOG, "reconnected wifi");

                    try {
                        //Thread.sleep(30000);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
                */
                //e.printStackTrace();
                isGet = false;
                reSetCount++;
                Log.d(TAG_LOG, "fail set Time count: " + reSetCount);

                //Count가 30이 되면 재부팅
                if (reSetCount > 3) {
//                    PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//                    pm.reboot("");

                    if(DEFAULT_ADDRESS.equals("http://55.111.150.201:8000/getInitTime")) {
                        DEFAULT_ADDRESS = "http://localhost:9000/getInitTime";
                    } else {
                        DEFAULT_ADDRESS = "http://55.111.150.201:8000/getInitTime";
                    }

                    reSetCount = 0;
                }

            } finally {
                if (conn != null)
                    conn.disconnect();
            }
        }
    }



    public static String sync_time_log(String time) {

        try {
            // sync_time.conf 에 설정된 시간  쓰기
            String time_sh_path = "/sdcard/";
            String time_sh_name = "sync_time.conf";
            File File = new File(time_sh_path + time_sh_name);

            BufferedWriter fw = new BufferedWriter(new FileWriter(File));
            fw.write(time);
            fw.flush();
            fw.close();

        } catch (Exception e){
            e.printStackTrace();
        }

        return "";

    }

//    public static void Log_udp_server(String log){
//
//        try {
//            String path = "/sdcard/";
//            File downld_path = new File(path);
//
//            File log_file = new File(path + "udp_server.log");
//
//            if (log_file.exists()==false) {
//                log_file.createNewFile();
//            } else {
//                if (log_file.length() > 1000000) {
//                    log_file.delete();
//                    log_file.createNewFile();
//                }
//            }
//
//            BufferedWriter buf = new BufferedWriter(new FileWriter(log_file, true));
//            buf.append(log);
//            buf.newLine();
//            buf.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//    }


}
